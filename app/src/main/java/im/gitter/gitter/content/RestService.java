package im.gitter.gitter.content;

import android.app.Service;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import im.gitter.gitter.LoginData;
import im.gitter.gitter.models.Room;
import im.gitter.gitter.models.User;
import im.gitter.gitter.network.ApiRequest;
import im.gitter.gitter.network.GroupsRequest;
import im.gitter.gitter.network.RoomsRequest;
import im.gitter.gitter.network.VolleySingleton;
import im.gitter.gitter.utils.ListUtils;

public class RestService extends Service {

    private static final String TAG = RestService.class.getSimpleName();

    // this service must be used via RestServiceHelper, hence the package private intent strings.
    protected static final String SERVICE_CALLBACK_INTENT_KEY = "SERVICE_CALLBACK";
    protected static final String REQUEST_ID_INTENT_KEY = "REQUEST_ID";
    protected static final String REQUEST_TYPE_INTENT_KEY = "REQUEST_TYPE";
    protected static final String ROOM_ID_INTENT_KEY = "ROOM_ID";
    protected static final String USER_ID_INTENT_KEY = "USER_ID";
    protected static final String GROUP_ID_INTENT_KEY = "GROUP_ID";
    protected static final String MESSAGE_INTENT_KEY = "MESSAGE";

    protected static final String ORIGINAL_INTENT_INTENT_KEY = "ORIGINAL_INTENT";

    private RequestQueue queue;
    private int workCounter;

    public enum RequestType {
        GetUserRooms,
        GetRoom,
        JoinRoom,
        LeaveRoom,
        GetUser,
        GetAdminGroups,
        GetUserGroups,
        GetRoomsForGroup,
        SendMessageToRoom,
        MarkAllAsRead
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.i(TAG, "Creating service");

        workCounter = 0;
        queue = VolleySingleton.getInstance(this).getRequestQueue();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        // we dont bind to activities as we want to outlive them!
        return null;
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        startingWork();

        WorkListener listener = new WorkListener() {
            @Override
            public void onFinish(int statusCode) {
                ResultReceiver callback = intent.getParcelableExtra(SERVICE_CALLBACK_INTENT_KEY);
                callback.send(statusCode, createBundle(intent));
                endingWork();
            }
        };

        RequestType requestType = (RequestType) intent.getSerializableExtra(REQUEST_TYPE_INTENT_KEY);
        switch (requestType) {
            case GetUserRooms:
                queue.add(new UserRoomsRequest(this, listener));
                break;
            case GetRoom:
                queue.add(new RoomRequest(this, "/v1/rooms/" + intent.getStringExtra(ROOM_ID_INTENT_KEY), listener));
                break;
            case JoinRoom:
                Map<String, String> json = new HashMap<>();
                json.put("id", intent.getStringExtra(ROOM_ID_INTENT_KEY));

                queue.add(new RoomRequest(
                        this,
                        Request.Method.POST,
                        "/v1/user/me/rooms",
                        new JSONObject(json),
                        listener));
                break;
            case LeaveRoom:
                queue.add(new VoidRequest(
                        this,
                        Request.Method.DELETE,
                        "/v1/rooms/" + intent.getStringExtra(ROOM_ID_INTENT_KEY) + "/users/" + new LoginData(this).getUserId(),
                        null,
                        listener));
                break;
            case GetUser:
                queue.add(new UserRequest(this, "/v1/user/" + intent.getStringExtra(USER_ID_INTENT_KEY ), listener));
                break;
            case GetAdminGroups:
                queue.add(new AdminGroupsRequest(this, listener));
                break;
            case GetUserGroups:
                queue.add(new UserGroupsRequest(this, listener));
                break;
            case GetRoomsForGroup:
                queue.add(new GroupRoomsRequest(this, intent.getStringExtra(GROUP_ID_INTENT_KEY), listener));
                break;
            case SendMessageToRoom:
                Map<String, String> messageJson = new HashMap<>();
                messageJson.put("text", intent.getStringExtra(MESSAGE_INTENT_KEY));
                String roomId = intent.getStringExtra(ROOM_ID_INTENT_KEY);
                queue.add(new VoidRequest(
                        this,
                        Request.Method.POST,
                        "/v1/rooms/" + roomId + "/chatMessages",
                        new JSONObject(messageJson),
                        listener));
                break;
            case MarkAllAsRead:
                queue.add(new VoidRequest(
                        this,
                        Request.Method.DELETE,
                        "/v1/user/me/rooms/" + intent.getStringExtra(ROOM_ID_INTENT_KEY) + "/unreadItems/all",
                        null,
                        listener));
                break;
            default:
                throw new IllegalArgumentException("Request type \"" + requestType + "\" not recognised");
        }

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "Destroying service");
    }

    private void startingWork() {
        workCounter += 1;
        Log.i(TAG, "Started work, " + workCounter + " concurrent job(s)");
    }

    private void endingWork() {
        workCounter -= 1;
        Log.i(TAG, "Finished work, " + workCounter + " concurrent job(s)");
        if (workCounter < 1) {
            Log.i(TAG, "Stopping self");
            stopSelf();
        }
    }

    private Bundle createBundle(Intent intent) {
        Bundle originalRequest = new Bundle();
        originalRequest.putParcelable(ORIGINAL_INTENT_INTENT_KEY, intent);
        return originalRequest;
    }

    private interface WorkListener {
        void onFinish(int statusCode);
    }

    private static class UserRoomsRequest extends RoomsRequest {

        UserRoomsRequest(Context context, WorkListener listener) {
            super(context, "/v1/rooms", new IdsListener(listener), new ErrorListener(listener));
        }

        protected void writeToDatabaseInBackground(ContentResolver contentResolver, ContentValues[] rooms, ContentValues[] users, ContentValues[] groups) {
            if (users.length > 0) {
                contentResolver.bulkInsert(ContentProvider.USERS_CONTENT_URI, users);
            }
            if (groups.length > 0) {
                contentResolver.bulkInsert(ContentProvider.GROUPS_CONTENT_URI, groups);
            }
            contentResolver.bulkInsert(ContentProvider.USER_ROOMS_CONTENT_URI, rooms);
        }
    }

    private static class GroupRoomsRequest extends RoomsRequest {

        private final String groupId;

        GroupRoomsRequest(Context context, String groupId, WorkListener listener) {
            super(context, "/v1/groups/" + groupId + "/rooms", new IdsListener(listener), new ErrorListener(listener));
            this.groupId = groupId;
        }

        protected void writeToDatabaseInBackground(ContentResolver contentResolver, ContentValues[] rooms, ContentValues[] users, ContentValues[] groups) {
            contentResolver.bulkInsert(ContentProvider.ROOMS_CONTENT_URI, rooms);

            // as we have just received the complete room list for a group, we can safely assume that
            // any other room that claims to be in this group is a liar. It was probably deleted on
            // the server so we should delete it too.
            //
            // There is a possible race condition here as our delete will be happening sometime after
            // our insert, but it would be rare and can be fixed by a pull to refresh.

            List<ContentValues> roomList = Arrays.asList(rooms);
            List<String> args = new ListUtils<ContentValues, String>().map(roomList, new ListUtils.Converter<ContentValues, String>() {
                @Override
                public String convert(ContentValues currentValue, int index, List<ContentValues> from) {
                    return currentValue.getAsString(Room._ID);
                }
            });

            StringBuilder builder = new StringBuilder((args.size() * 2) - 1);
            for (int i = 0; i < args.size(); i++) {
                if (i > 0) {
                    builder.append(",");
                }
                builder.append("?");
            }

            args.add(0, groupId);
            contentResolver.delete(
                    ContentProvider.ROOMS_CONTENT_URI,
                    Room.GROUP_ID + " = ? AND " + Room._ID + " NOT IN (" + builder.toString() + ")",
                    args.toArray(new String[args.size()])
            );

        }
    }

    private static class AdminGroupsRequest extends GroupsRequest {

        AdminGroupsRequest(Context context, WorkListener listener) {
            super(context, "/v1/groups?type=admin", new IdsListener(listener), new ErrorListener(listener));
        }

        @Override
        protected void writeToDatabaseInBackground(ContentResolver contentResolver, ContentValues[] groups) {
            contentResolver.bulkInsert(ContentProvider.ADMIN_GROUPS_CONTENT_URI, groups);
        }
    }

    private static class UserGroupsRequest extends GroupsRequest {

        UserGroupsRequest(Context context, WorkListener listener) {
            super(context, "/v1/groups", new IdsListener(listener), new ErrorListener(listener));
        }

        @Override
        protected void writeToDatabaseInBackground(ContentResolver contentResolver, ContentValues[] groups) {
            contentResolver.bulkInsert(ContentProvider.USER_GROUPS_CONTENT_URI, groups);
        }
    }

    private static class RoomRequest extends im.gitter.gitter.network.RoomRequest {

        RoomRequest(Context context, String path, WorkListener listener) {
            this(context, Method.GET, path, null, listener);
        }

        RoomRequest(Context context, int method, String path, JSONObject jsonRequest, WorkListener listener) {
            super(context, method, path, jsonRequest, new IdListener(listener), new ErrorListener(listener));
        }

        @Override
        protected void writeToDatabaseInBackground(ContentResolver contentResolver, ContentValues room, ContentValues user, ContentValues group) {
            if (user != null) {
                contentResolver.bulkInsert(ContentProvider.USERS_CONTENT_URI, new ContentValues[] { user });
            }
            if (group != null) {
                contentResolver.bulkInsert(ContentProvider.GROUPS_CONTENT_URI, new ContentValues[] { group });
            }
            contentResolver.bulkInsert(ContentProvider.ROOMS_CONTENT_URI, new ContentValues[] { room });
        }
    }

    private static class UserRequest extends ApiRequest<String> {

        ContentResolver contentResolver;

        UserRequest(Context context, String path, WorkListener listener) {
            super(context, path, new IdListener(listener), new ErrorListener(listener));
            contentResolver = context.getContentResolver();
        }

        @Override
        protected String parseJsonInBackground(String jsonString) throws JSONException {
            JSONObject jsonObject = new JSONObject(jsonString);
            User user = new ModelFactory().createUser(jsonObject);

            contentResolver.bulkInsert(ContentProvider.USERS_CONTENT_URI, new ContentValues[] { user.toContentValues() });

            return user.getId();
        }
    }


    private static class VoidRequest extends ApiRequest<Void> {
        VoidRequest(Context context, int method, String path, JSONObject jsonRequest, WorkListener listener) {
            super(context, method, path, jsonRequest, new VoidListener(listener), new ErrorListener(listener));
        }

        @Override
        protected Void parseJsonInBackground(String jsonString) throws JSONException {
            return null;
        }
    }

    private static class IdListener implements Response.Listener<String> {

        private WorkListener listener;

        IdListener(WorkListener listener) {
            this.listener = listener;
        }

        @Override
        public void onResponse(String id) {
            listener.onFinish(200);
        }
    }

    private static class IdsListener implements Response.Listener<Set<String>> {

        private WorkListener listener;

        IdsListener(WorkListener listener) {
            this.listener = listener;
        }

        @Override
        public void onResponse(Set<String> ids) {
            listener.onFinish(200);
        }
    }

    private static class VoidListener implements Response.Listener<Void> {

        private WorkListener listener;

        VoidListener(WorkListener listener) {
            this.listener = listener;
        }

        @Override
        public void onResponse(Void v) {
            listener.onFinish(200);
        }
    }

    private static class ErrorListener implements Response.ErrorListener {

        private WorkListener listener;

        public ErrorListener(WorkListener listener) {
            this.listener = listener;
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            int statusCode = error.networkResponse != null ? error.networkResponse.statusCode : 600;
            this.listener.onFinish(statusCode);
        }
    }
}
